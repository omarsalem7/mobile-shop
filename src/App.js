import Home from './pages/home/home';
import ProductDetails from './pages/product_details/productDetails';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import 'swiper/css/bundle';
import './App.css';
import Error from './pages/error/error';
function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/details/:id" element={<ProductDetails />} />
        <Route exact path="*" element={<Error />} />
      </Routes>
    </Router>
  );
}

export default App;
