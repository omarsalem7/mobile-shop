import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import { slidersApi } from './api/slider';
import { productsApi } from './api/products';

export const store = configureStore({
  reducer: {
    [slidersApi.reducerPath]: slidersApi.reducer,
    [productsApi.reducerPath]: productsApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      slidersApi.middleware,
      productsApi.middleware
    ),
});
setupListeners(store.dispatch);
