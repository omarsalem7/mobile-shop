import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const slidersApi = createApi({
  reducerPath: 'slidersApi',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://63189f2cf6b281877c71eab0.mockapi.io',
  }),
  endpoints: (builder) => ({
    getSliders: builder.query({
      query: () => '/slider',
    }),
  }),
});

export const { useGetSlidersQuery } = slidersApi;
