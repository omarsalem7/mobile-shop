import React from 'react';
import { useParams } from 'react-router-dom';
import { useGetProductsQuery } from '../../redux/api/products';
import Navbar from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';
import Products from '../../components/products/products';
import styles from './productDetails.module.css';
import ViewProduct from '../../components/viewProduct/viewProduct';
import DetailInfo from '../../components/detailInfo/detailInfo';
import Spinner from '../../components/spinner/spinner';

const ProductDetails = () => {
  const { id } = useParams();
  const { data, isLoading, isSuccess } = useGetProductsQuery({
    pollingInterval: 1,
  });

  let content;
  if (isSuccess) {
    content = (
      <>
        <div className={styles.details}>
          <ViewProduct product={data.find((prod) => prod.id === id)} />
          <DetailInfo product={data.find((prod) => prod.id === id)} />
        </div>
        <Products />
      </>
    );
  } else if (isLoading) {
    content = <Spinner />;
  }
  return (
    <div>
      <Navbar />
      {content}
      <Footer />
    </div>
  );
};

export default ProductDetails;
