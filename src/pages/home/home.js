import React from 'react';
import Navbar from '../../components/navbar/navbar';
import Carousel from '../../components/carousel/carousel';
import Products from '../../components/products/products';
import Footer from '../../components/footer/footer';


const Home = () => {
  return (
    <>
      <Navbar />
      <Carousel />
      <Products />
      <Footer />
    </>
  );
};

export default Home;
