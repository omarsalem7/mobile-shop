import React from 'react';
import styles from './choose.module.css';

const Choose = ({ children, ...rest }) => {
  return (
    <button {...rest} className={styles.infoChoose}>
      {children}
    </button>
  );
};

export default Choose;
