import React from 'react';
import appleIcon from '../../assets/icons/apple-sm.png';
import './footer.css';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-row1">
        {new Array(4)
          .fill()
          .map((_, indx) => indx + 1)
          .map((num) => (
            <img
              key={Math.random() * 100}
              src={appleIcon}
              alt={`apple${num}`}
            />
          ))}
      </div>
      <div className="footer-row2">Copy Right reserved to @omarsalem7</div>
    </footer>
  );
};

export default Footer;
