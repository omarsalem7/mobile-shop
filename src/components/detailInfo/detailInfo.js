import React, { useState } from 'react';
import { AiOutlinePlusCircle, AiOutlineMinusCircle } from 'react-icons/ai';
import './detailInfo.css';
import cibIcon from '../../assets/icons/cib-sm.png';
import Choose from '../choose/choose';
import Rate from '../rate/rate';

const DetailInfo = ({ product }) => {
  const [memoryData] = useState([
    { id: 1, storage: '128GB', available: true },
    { id: 2, storage: '2568GB', available: false },
    { id: 3, storage: '512GB', available: false },
    { id: 4, storage: '1TB', available: false },
  ]);
  const btnColor = product?.inStock > 0 ? 'var(--blue)' : 'var(--black)';
  return (
    <div className="info">
      <small>{product?.name}</small>
      <Rate starsNumber={product?.numberOfStars} />
      <p className="info-price">${product?.price}</p>
      {product?.inStock ? (
        <small style={{ color: 'green', fontSize: '10px' }}>in Stock</small>
      ) : (
        <small style={{ color: 'red', fontSize: '10px' }}>out Of Stock</small>
      )}
      <div className="increase-decrease">
        <AiOutlinePlusCircle size={20} /> <span>0 </span>
        <AiOutlineMinusCircle size={20} />
      </div>

      <div className="info-row">
        <Choose disabled style={{ color: '#C3C3C3' }}>
          <p>full price</p>
        </Choose>
        <Choose>
          <p>installments</p>
        </Choose>
      </div>
      <div className="info-row">
        <Choose>
          <img src={cibIcon} alt="cib" />
        </Choose>
        <Choose>
          <img src={cibIcon} alt="cib" />
        </Choose>
        <Choose>
          <img src={cibIcon} alt="cib" />
        </Choose>
        <Choose>
          <img src={cibIcon} alt="cib" />
        </Choose>
      </div>
      <small>Memory</small>
      <div className="info-row">
        {memoryData.map(({ id, available, storage }) => {
          if (available) {
            return (
              <Choose key={id}>
                <div>
                  <span>{storage}</span>
                  <span style={{ display: 'block' }}>$999.00</span>
                </div>
              </Choose>
            );
          } else {
            return (
              <Choose key={id} style={{ color: '#C3C3C3' }}>
                <div>
                  <span>{storage}</span>
                  <span style={{ display: 'block' }}>$999.00</span>
                </div>
              </Choose>
            );
          }
        })}
      </div>
      <small>Color</small>
      <div className="info-row">
        <Choose>
          <div>
            <small>Silver</small>
            <span
              style={{ background: '#EDEDED' }}
              className="info-color"
            ></span>
          </div>
        </Choose>
        <Choose>
          <div>
            <small>Blue</small>
            <span className="info-color"></span>
          </div>
        </Choose>
      </div>
      <button
        className="info-btn"
        style={{
          background: btnColor,
        }}
      >
        Add to Cart
      </button>
    </div>
  );
};

export default DetailInfo;
