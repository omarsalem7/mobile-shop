import React from 'react';
import { NavLink } from 'react-router-dom';

const ProductItem = ({ product }) => {
  return (
    <>
      <div key={product.id} className="product-item">
        <NavLink to={`/details/${product.id}`}>
          <div className="product-content">
            <div className="product-content-img">
              <img src={product.img[0]} alt={product.name} />
            </div>
            <div className="product-content">
              <p>{product.name}</p>
              <p>${product.price}</p>
              {product.inStock ? (
                <p style={{ color: 'green' }}>in Stock</p>
              ) : (
                <p style={{ color: 'red' }}>out Of Stock</p>
              )}
            </div>
          </div>
        </NavLink>
      </div>
    </>
  );
};

export default ProductItem;
