import './products.css';
import { useGetProductsQuery } from '../../redux/api/products';
import ProductItem from '../productItem/productItem';
import Spinner from '../spinner/spinner';

const Products = () => {
  const { data, isLoading, isSuccess } = useGetProductsQuery({
    pollingInterval: 50000000,
  });
  let content;

  if (isLoading) {
    content = <Spinner />;
  } else if (isSuccess) {
    content = data
      .filter((item) => item.id <= 4)
      .map((product) => <ProductItem key={product.id} product={product} />);
  }
  return (
    <div>
      <h3 className="title">Explore Products</h3>
      <div className="products">{content}</div>
    </div>
  );
};

export default Products;
