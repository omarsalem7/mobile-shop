import React from 'react';
import styles from './viewProduct.module.css';

const ViewProduct = ({ product }) => {
  return (
    <div className={styles.ViewProduct}>
      <div className={styles.productImgs}>
        {product.img.map((img) => (
          <img src={img} key={Math.random() * 100} alt="views" />
        ))}
      </div>
      <div className={styles.mainImage}>
        <img src={product.img[0]} alt="view" />
      </div>
    </div>
  );
};

export default ViewProduct;
