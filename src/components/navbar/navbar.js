import React from 'react';
import styles from './navbar.module.css';
import appleIcon from '../../assets/icons/apple-sm.png';
import profileIcon from '../../assets/icons/profile-sm.png';
import searchIcon from '../../assets/icons/search-sm.png';
import bagIcon from '../../assets/icons/bag-sm.png';
import menuIcon from '../../assets/icons/menu-sm.png';

const Navbar = () => {
  return (
    <nav className={styles.nav}>
      <span>
        <img src={appleIcon} alt="apple logo" />
      </span>
      <ul>
        <li>Mac</li>
        <li>iPad</li>
        <li>iPhone</li>
        <li>Watch</li>
        <li>TV</li>
        <li>Accessories</li>
        <li>Offers</li>
        <li>Support</li>
        <li>Services</li>
        <li>Location</li>
      </ul>
      <ul>
        <li>
          <img src={searchIcon} alt="search" />
        </li>
        <li>
          <img src={bagIcon} alt="bag" />
        </li>
        <li>
          <img src={profileIcon} alt="profile" />
        </li>
      </ul>
      <span className={styles.mobileMenu}>
        <img src={menuIcon} alt="humberger menu" />
      </span>
    </nav>
  );
};

export default Navbar;
