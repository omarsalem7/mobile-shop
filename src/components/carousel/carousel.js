import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Mousewheel, Keyboard } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import './carousel.css';
import { useGetSlidersQuery } from '../../redux/api/slider';

function Carousel() {
  const { data } = useGetSlidersQuery({
    pollingInterval: 500000,
  });
  return (
    <>
      <Swiper
        cssMode={true}
        navigation={true}
        pagination={true}
        mousewheel={true}
        loop={true}
        keyboard={true}
        modules={[Navigation, Pagination, Mousewheel, Keyboard]}
        className=""
      >
        {data?.map(({ img, id }) => (
          <SwiperSlide key={id}>
            <img src={img} alt={`swiper-${id}`} />
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
export default Carousel;
