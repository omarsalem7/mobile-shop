import React from 'react';
import yStar from '../../assets/icons/yellow-star.png';
import nStar from '../../assets/icons/n-star.png';

const Rate = ({ starsNumber }) => {
  return (
    <div className="rate">
      {new Array(5)
        .fill()
        .map((_, indx) => indx + 1)
        .map((star) => {
          if (starsNumber >= star) {
            return <img key={Math.random() * 100} src={yStar} alt="y star" />;
          } else {
            return <img key={Math.random() * 100} src={nStar} alt="n star" />;
          }
        })}
    </div>
  );
};

export default Rate;
